GCC 4.8.4


1) Descompresión del código fuente

mkdir /usr/local/GCC
tar xjf gcc-4.8.4.tar.bz2
cd gcc-4.8.4/

2) Instalación de las dependencias

yum install gcc
yum install gmp-devel
yum install mpfr-devel
yum install libmpc-devel
yum install libtool
yum install gcc-c++
yum install texinfo
yum install glibc-devel.i686

3) Compilación e instalación

./configure --prefix=/usr/local/GCC --enable-languages="c,c++,objc"
make -j3 ; make -j3 install

